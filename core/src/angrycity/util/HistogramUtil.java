package angrycity.util;

import java.util.HashMap;
import java.util.Map;

public class HistogramUtil {
    private Map<Object, Integer> objectIntegerMap = new HashMap<>();

    public void put(Object o) {
        if (objectIntegerMap.containsKey(o)) {
            objectIntegerMap.put(o, objectIntegerMap.get(o) + 1);
        } else {
            objectIntegerMap.put(o, 1);
        }
    }

    public void printResult() {
        for (Object o : objectIntegerMap.keySet()) {
            System.out.println("key = " + o + " " + "value = " + objectIntegerMap.get(o));
        }
    }
}
