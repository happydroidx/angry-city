package angrycity.util;

public class PlatformMonitor {
    private PlatformMonitor() {

    }

//HARDWARE

    public static long getFreeMemory() {
        com.sun.management.OperatingSystemMXBean bean =
                (com.sun.management.OperatingSystemMXBean)
                        java.lang.management.ManagementFactory.getOperatingSystemMXBean();
        return bean.getFreePhysicalMemorySize();
    }

    public static String getArchCpu() {
        com.sun.management.OperatingSystemMXBean bean =
                (com.sun.management.OperatingSystemMXBean)
                        java.lang.management.ManagementFactory.getOperatingSystemMXBean();
        return bean.getArch();
    }

    public static double getCpuUsage() {
        com.sun.management.OperatingSystemMXBean bean =
                (com.sun.management.OperatingSystemMXBean)
                        java.lang.management.ManagementFactory.getOperatingSystemMXBean();
        return bean.getSystemCpuLoad();

    }

    public static String getOsName() {
        return System.getProperty("os.name");
    }

    public static int getNumberOfCore() {
        return Runtime.getRuntime().availableProcessors();
    }

//OS

    private static String OS = System.getProperty("os.name").toLowerCase();

    public static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }

    public static boolean isMac() {
        return (OS.indexOf("mac") >= 0);
    }

    public static boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    }

    public static boolean isSolaris() {
        return (OS.indexOf("sunos") >= 0);
    }

    public static String generateReport() {
        return "Os name: " + getOsName() + "\n" +
                "Os version " + System.getProperty("os.version") + "\n" +
                "Free memory: " + getFreeMemory() / 1024 / 1024 + "\n" +
                "Arch cpu: " + getArchCpu() + "\n" +
                "Number of Core: " + getNumberOfCore() + "\n" +
                "User lang " + System.getProperty("user.language") + "\n" +
                "User country: " + System.getProperty(System.getProperty("user.country")) + "\n";
    }
}
