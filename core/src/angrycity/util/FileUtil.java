package angrycity.util;

import java.io.File;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileUtil {

    public static String getFileExtension(String pathToFile) {
        int i = pathToFile.lastIndexOf('.');
        if (i > 0) {
            return pathToFile.substring(i + 1);
        } else {
            return null;
        }
    }

    public static String getFileExtension(File file) {
        return getFileExtension(file.getAbsolutePath());
    }

    public static String getFileName(String pathToFile) {
        return new File(pathToFile).getName().replaceFirst("[.][^.]+$", "");
    }

    public static String getFileName(File file) {
        return file.getName().replaceFirst("[.][^.]+$", "");
    }

    public static URL getUrl(String pathToFile) {
        URL url = null;
        try {
            url = new File(pathToFile).toURI().toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    public static List<File> getAllFilesWithExtension(String path, String extension, List<File> resultFilesList) {
        if (resultFilesList == null) {
            resultFilesList = new ArrayList<>();
        }
        File[] files = new File(path).listFiles();
        if (files == null) {
            return null;
        }
        for (File file : files) {
            String subPath = file.getAbsolutePath();
            if (file.isDirectory()) {
                getAllFilesWithExtension(subPath, extension, resultFilesList);
            } else {
                String fileExtension = getFileExtension(subPath);
                if ((fileExtension != null) && (!fileExtension.isEmpty()) && (fileExtension.equals(extension))) {
                    resultFilesList.add(file);
                }
            }
        }
        return resultFilesList;
    }

    public static String readFileAsString(String path) {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            return new String(encoded, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String readFileAsString(String path, Charset encoding) {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            return new String(encoded, encoding);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getRelativePath(String absolutePath) {
        String base = new File(".").getAbsolutePath();
        String relative = new File(base).toURI().relativize(new File(absolutePath).toURI()).getPath();
        return relative;
    }

    public static List<String> readFileAsListStrings(String pathToFile) {
        try {
            Scanner s = new Scanner(new File(pathToFile));
            ArrayList<String> list = new ArrayList<>();
            while (s.hasNextLine()) {
                list.add(s.nextLine());
            }
            s.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String readFileToString(String pathToFile) {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(pathToFile));
            return new String(encoded, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void writeStringToFile(String pathToFile, String text) {
        try (PrintWriter out = new PrintWriter(pathToFile)) {
            out.println(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getAllFiles(String directoryName, List<File> files) {
        File directory = new File(directoryName);

        // get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()) {
                files.add(file);
            } else if (file.isDirectory()) {
                getAllFiles(file.getAbsolutePath(), files);
            }
        }
    }
}
