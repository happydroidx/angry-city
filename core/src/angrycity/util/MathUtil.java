package angrycity.util;

import com.badlogic.gdx.math.Vector2;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class MathUtil {
    public static final float PI = (float) (Math.PI);
    public static final float DOUBLE_PI = (float) (Math.PI * 2);

    public static int maxRandomValue = 800;

    //use for rotated
    public static float newXTurn(float x, float y, float angle) {
        return x * FastMath.cos(angle) - y * FastMath.sin(angle);
    }

    public static float newYTurn(float x, float y, float angle) {
        return x * FastMath.sin(angle) + y * FastMath.cos(angle);
    }

    public static final Random random = new Random();

    public static float getRandomFloat(float min, float max) {
        return min + random.nextFloat() * (max - min);
    }

    public static int getRandomInt(int min, int max) {
        return min + random.nextInt(max - min + 1);
    }

    public static float distance(Vector2 vector1, Vector2 vector2) {
        return (float) Math.sqrt(((vector1.x - vector2.x) * (vector1.x - vector2.x) + (vector1.y - vector2.y) * (vector1.y - vector2.y)));
    }

    public static float squareDistance(Vector2 vector1, Vector2 vector2) {
        return ((vector1.x - vector2.x) * (vector1.x - vector2.x) + (vector1.y - vector2.y) * (vector1.y - vector2.y));
    }

    public static float getAngleBetweenVectors(Vector2 vec1, Vector2 vec2) {
        return (float) Math.atan2(vec2.y - vec1.y, vec2.x - vec1.x);
    }

    //random choose one value from keys of map, use chance by float
    public static String defineRandomRange(Map<String, Float> mapOfChance) {
        float summ = 0;
        for (Float aFloat : mapOfChance.values()) {
            summ += aFloat;
        }
        float randomNumber = getRandomFloat(0, summ);
        String[] strings = mapOfChance.keySet().toArray(new String[mapOfChance.size()]);
        float currentSummOfShance = 0;
        for (int i = 0; i < strings.length; i++) {
            currentSummOfShance += mapOfChance.get(strings[i]);
            if ((randomNumber < currentSummOfShance)) {
                return strings[i];
            }
        }

        int randomIndex = MathUtil.random.nextInt(strings.length);
        return strings[randomIndex];
    }

    public static float getDiffBetweenAngles(float angle, float targetAngle) {
        float diff;
        diff = (targetAngle - angle) % DOUBLE_PI;
        if (diff < 0) {
            diff += DOUBLE_PI;
        }
        if (diff > Math.PI) {
            return -(DOUBLE_PI - diff);
        } else {
            return diff;
        }
    }

    public static float degreesToRad(float degrees) {
        return degrees * PI / 180f;
    }

    public static float radToDegrees(float rad) {
        return rad * 180f / PI;
    }

    public static boolean isRectanglesOverlapped(float leftA, float bottomA, float rightA, float topA, float leftB, float bottomB, float rightB, float topB) {
        return ((leftA < rightB) && (rightA > leftB) && (topA > bottomB) && (bottomA < topB));
    }

    public static boolean isCircleAndRectangleOverlapped(float xCircle, float yCircle, float radius, float xRect, float yRect, float width, float height) {
        float circleDistanceX = Math.abs(xCircle - xRect);
        float circleDistanceY = Math.abs(yCircle - yRect);
        if (circleDistanceX > (width * 0.5f + radius)) {
            return false;
        }
        if (circleDistanceY > (height * 0.5f + radius)) {
            return false;
        }
        if (circleDistanceX <= (width * 0.5f)) {
            return true;
        }
        if (circleDistanceY <= (height * 0.5f)) {
            return true;
        }
        float cornerDistance_sq = (circleDistanceX - width * 0.5f) * ((circleDistanceX - width * 0.5f)) +
                (circleDistanceY - height * 0.5f) * (circleDistanceY - height * 0.5f);
        return (cornerDistance_sq <= (radius * radius));
    }

    public static boolean isPointInCircle(float pointX, float pointY, float circleX, float circleY, float radius) {
        return (pointX - circleX) * (pointX - circleX) + (pointY - circleY) * (pointY - circleY) < radius * radius;
    }

    public static boolean isLinesOverlapped(Vector2 l1p1, Vector2 l1p2, Vector2 l2p1, Vector2 l2p2) {
        float q = (l1p1.y - l2p1.y) * (l2p2.x - l2p1.x) - (l1p1.x - l2p1.x) * (l2p2.y - l2p1.y);
        float d = (l1p2.x - l1p1.x) * (l2p2.y - l2p1.y) - (l1p2.y - l1p1.y) * (l2p2.x - l2p1.x);
        if (d == 0) {
            return false;
        }
        float r = q / d;
        q = (l1p1.y - l2p1.y) * (l1p2.x - l1p1.x) - (l1p1.x - l2p1.x) * (l1p2.y - l1p1.y);
        float s = q / d;
        if (r < 0 || r > 1 || s < 0 || s > 1) {
            return false;
        }
        return true;
    }

    public static Vector2 computeCentroid(List<Vector2> vertices) {
        Vector2 centroid = new Vector2();
        double signedArea = 0.0;
        double x0 = 0; // Current vertex X
        double y0 = 0; // Current vertex Y
        double x1 = 0; // Next vertex X
        double y1 = 0; // Next vertex Y
        double a = 0;  // Partial signed area

        // For all vertices except last
        for (int i = 0; i < vertices.size() - 1; ++i) {
            x0 = vertices.get(i).x;
            y0 = vertices.get(i).y;
            x1 = vertices.get(i + 1).x;
            y1 = vertices.get(i + 1).y;
            a = x0 * y1 - x1 * y0;
            signedArea += a;
            centroid.x += (x0 + x1) * a;
            centroid.y += (y0 + y1) * a;
        }

        // Do last vertex separately to avoid performing an expensive
        // modulus operation in each iteration.
        x0 = vertices.get(vertices.size() - 1).x;
        y0 = vertices.get(vertices.size() - 1).y;
        x1 = vertices.get(0).x;
        y1 = vertices.get(0).y;
        a = x0 * y1 - x1 * y0;
        signedArea += a;
        centroid.x += (x0 + x1) * a;
        centroid.y += (y0 + y1) * a;

        signedArea *= 0.5;
        centroid.x /= (6.0 * signedArea);
        centroid.y /= (6.0 * signedArea);

        return centroid;
    }

    public static int getYourPositiveFunctionRandomNumber(int startIndex, int stopIndex) {
        //Generate a random number whose value ranges from 0.0 to the sum of the values of yourFunction for all the possible integer return values from startIndex to stopIndex.
        double randomMultiplier = 0;
        for (int i = startIndex; i <= stopIndex; i++) {
            randomMultiplier += yourFunction(i);//yourFunction(startIndex) + yourFunction(startIndex + 1) + .. yourFunction(stopIndex -1) + yourFunction(stopIndex)
        }
        Random r = new Random();
        double randomDouble = r.nextDouble() * randomMultiplier;

        //For each possible integer return value, subtract yourFunction value for that possible return value till you get below 0.  Once you get below 0, return the current value.
        int yourFunctionRandomNumber = startIndex;
        randomDouble = randomDouble - yourFunction(yourFunctionRandomNumber);
        while (randomDouble >= 0) {
            yourFunctionRandomNumber++;
            randomDouble = randomDouble - yourFunction(yourFunctionRandomNumber);
        }

        return yourFunctionRandomNumber;
    }

    private static double yourFunction(int value) {
        return (float) value / maxRandomValue;
    }

    public static void lerpVectorFixedSpeed(Vector2 source, Vector2 target, float speed) {
        Vector2 addLength = new Vector2(target.x - source.x, target.y - source.y);
        if (addLength.len2() < speed * speed) {
            source.x = target.x;
            source.y = target.y;
        } else {
            addLength.nor();
            addLength.scl(speed);
            source.add(addLength);
        }
    }

    public static Vector2 getClosesPointForCircle(Vector2 point, Vector2 center, float radius) {
        double vX = point.x - center.x;
        double vY = point.y - center.y;
        double magV = Math.sqrt(vX * vX + vY * vY);
        double aX = center.x + vX / magV * radius;
        double aY = center.y + vY / magV * radius;
        return new Vector2((float) aX, (float) aY);
    }

    public static <T> T getRandomObjectFromList(List<T> objects) {
        return objects.get(random.nextInt(objects.size()));
    }

    public static void main(String args[]) {
        System.out.println(getClosesPointForCircle(new Vector2(10.1f, 10.1f), new Vector2(10, 10), 0.1f));
    }
}
