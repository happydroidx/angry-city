package angrycity.util.gson;

import angrycity.logic.heroes.Hero;
import com.google.gson.*;

import java.lang.reflect.Type;

public class JsonEntityAdapter implements JsonSerializer<JsonEntity>, JsonDeserializer<JsonEntity> {

    private static final String CLASSNAME = "CLASSNAME";
    private static final String INSTANCE = "INSTANCE";

    @Override
    public JsonElement serialize(JsonEntity src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject retValue = new JsonObject();
        String className = src.getClass().getName();
        retValue.addProperty(CLASSNAME, className);
        JsonElement elem = context.serialize(src);
        retValue.add(INSTANCE, elem);
        return retValue;
    }

    @Override
    public JsonEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        JsonPrimitive prim = (JsonPrimitive) jsonObject.get(CLASSNAME);
        String className = prim.getAsString();

        Class<?> klass = null;
        try {
            klass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new JsonParseException(e.getMessage());
        }
        return context.deserialize(jsonObject.get(INSTANCE), klass);
    }

    public static void main(String[] args) {
        JsonEntity animals[] = new JsonEntity[]{new Hero("Hulk", 10, 2349, 100, 100, 5), new Setting("Setting", 0.5f, 5)};
        Gson gsonExt = null;
        {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(JsonEntity.class, new JsonEntityAdapter());
            gsonExt = builder.create();
        }
        for (JsonEntity animal : animals) {
            String animalJson = gsonExt.toJson(animal, JsonEntity.class);
            System.out.println("serialized with the custom serializer:" + animalJson);
            JsonEntity animal2 = gsonExt.fromJson(animalJson, JsonEntity.class);
            System.out.println("serialized with the custom serializer:" + animal2);
        }
    }
}