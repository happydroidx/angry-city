package angrycity.util.gson;

public class Setting implements JsonEntity {

    private String gameVersion;
    private float musicVolume;
    private int numberOfPlays;


    public Setting(String gameVersion, float musicVolume, int numberOfPlays) {
        this.gameVersion = gameVersion;
        this.musicVolume = musicVolume;
        this.numberOfPlays = numberOfPlays;
    }
}
