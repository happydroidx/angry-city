package angrycity.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TextUtil {
    private static NumberFormat format = DecimalFormat.getInstance();

    private TextUtil() {

    }

    static {
        format.setMinimumFractionDigits(0);
        format.setMaximumFractionDigits(2);
    }

    public static String format(float number) {
        return format.format(number).replaceAll(",", ".");
    }
}
