package angrycity.logic;

import angrycity.logic.heroes.Hero;

import java.util.ArrayList;
import java.util.List;

//Содержит текущий прогресс игрока, героев игрока и их прокачку
public class Game {
    private int stepNumber;
    private List<Hero> heroes = new ArrayList<>();

    public Game() {
        loadHeroes();
        loadVillain();
        loadCity();
    }

    private void loadHeroes() {

    }

    private void loadVillain() {

    }

    private void loadCity() {

    }

    public void makeStep() {
        stepNumber++;
        System.out.println(stepNumber);
    }

}
