package angrycity.logic.heroes;

import angrycity.util.gson.JsonEntity;

public class Hero implements JsonEntity {

    private String name;

    private int level;

    private float xp;

    private float hp;

    private float energy;

    private float stress;


    public Hero(String name, int level, float xp, float hp, float energy, float stress) {
        this.name = name;
        this.level = level;
        this.xp = xp;
        this.hp = hp;
        this.energy = energy;
        this.stress = stress;
    }
}
