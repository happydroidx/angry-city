package angrycity.logic.sim;

public class History {

    private int[][] history;

    public History(int run) {
        history = new int[run][2];
    }

    public void printHistory() {
        for (int i = 0; i < history.length; i++) {
            System.out.println(history[i][0] + ". " + history[i][1]);
        }
    }

    public void setHistory(int i, int value) {
        this.history[i][0] = i + 1;
        this.history[i][1] = value;
    }
}
