package angrycity.logic.sim;

public class Catastrophe {

    private int dead(double population) {
        if (population < (int) (Simulation.POP_MAX / 1.5)) {
            return (int) (Math.random() * 10);
        } else {
            return (int) ((population / Simulation.POP_MAX) * 10);
        }
    }

    public boolean event(int population) {
        if (dead(population) >= 6) {
            return true;
        } else {
            return false;
        }
    }

    public int victims() {
        return (int) (Math.random() * 10);
    }
}
