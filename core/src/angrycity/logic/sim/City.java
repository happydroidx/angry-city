package angrycity.logic.sim;

public class City {

    private int population;

    public City(int start) {
        this.population = start;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    private double growth() {
        double percent = (int) (Math.random() * 100);
        if (percent >= 26 && percent <= 60) {
            return 0;
        } else {
            growth();
        }
        return percent;
    }

    public void popGrowth(double population) {
        this.population = (int) (population * (1 + growth() / 1000));
    }
}
