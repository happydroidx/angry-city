package angrycity.logic.sim;

public class Simulation {

    private static final int POP_START = 130000;
    public static final int POP_MAX = 2700000;
    private static final int RUN = 1000;

    public static void main(String[] args) {

        City city = new City(POP_START);
        Catastrophe crash = new Catastrophe();
        History statistic = new History(RUN);

        for (int i = 0; i < RUN; i++) {
            city.popGrowth(city.getPopulation());
            statistic.setHistory(i, city.getPopulation());
            System.out.println(i + 1 + ". Количество населения: " + city.getPopulation() + ".");

            if (crash.event(city.getPopulation())) {
                int dead = ((city.getPopulation() / 100) * crash.victims());
                city.setPopulation(city.getPopulation() - dead);
                System.out.println("\tПроизошла катастрофа!!!\n\tПогибло:" + dead + ".");
            } else {
                System.out.println("\tСобытий не было.");
            }
        }

        statistic.printHistory();
    }
}
