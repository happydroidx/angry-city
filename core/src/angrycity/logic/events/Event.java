package angrycity.logic.events;

public enum Event {

    CityCatastrophe,
    NewHeroes,
    NewVillain,
    VillainAttack,
    None

}
